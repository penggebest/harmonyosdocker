# 华为鸿蒙OS辅助Dockerfile

#### 介绍
华为鸿蒙OS辅助Dockerfile，构造环境，可以方便开发者快速的体验编译
 

#### 安装教程

1. 安装docker  
2. git clone 
3. 根目录打包 docker build -t python-centos:3.8.5rc2 .

#### 使用说明

1.  docker run -itd --name python --restart always --privileged=true  -v D:\harmonyos\src\code-1.0:/harmonyos  python-centos:3.8.5rc2 /usr/sbin/init

#### TODO

1.改进代码 