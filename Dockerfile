FROM centos:centos7.6.1810
MAINTAINER yigesuren 530566495@qq.com
# RUN yum install -y wget bzip2
# RUN yum install -y git gcc
# wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
# [参考的原docker文件来源](https://github.com/perfectstorm88/docker-centos7-python3)
# [linux 下用shell 写入多行文本](https://blog.csdn.net/zhaowei3828/article/details/19114157)
# [python pip修改安装镜像源](https://blog.csdn.net/jeffery0207/article/details/82965910)
# [Docker -- 使用Docker构建基于centos7镜像的python3.x环境](https://blog.csdn.net/Aeve_imp/article/details/101461488)
ENV PYTHON_VERSION "3.8.5"
##############################################
# 基于centos7构建python3运行环境
# 构建命令: 在Dockerfile文件目录下执行 docker build -t python-centos:3.8.5rc2 .
# 容器启动命令: docker run -itd --name python --restart always --privileged=true  -v D:\harmonyos\src\code-1.0:/harmonyos  python-centos:3.8.5 /usr/sbin/init
# 进入容器：docker exec -it python /bin/bash
##############################################
RUN set -ex \
    # 预安装所需组件
    && yum install -y wget tar libffi-devel zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gcc gcc-c++ kernel-devel make initscripts \
    && wget https://npm.taobao.org/mirrors/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz \
    && tar -zxvf Python-${PYTHON_VERSION}.tgz \
    && cd Python-${PYTHON_VERSION} \
    && ./configure prefix=/usr/local/python3 \
    && make \
    && make install \
    && make clean \
    && rm -rf /Python-${PYTHON_VERSION}* \
    && yum install -y epel-release \
    && yum install -y python-pip
# # 设置默认为python3
RUN set -ex \
    # 备份旧版本python
    && mv /usr/bin/python /usr/bin/python27 \
    && mv /usr/bin/pip /usr/bin/pip-python27 \
    # 配置默认为python3
    && ln -s /usr/local/python3/bin/python3 /usr/bin/python \
    && ln -s /usr/local/python3/bin/pip3 /usr/bin/pip
# 修复因修改python版本导致yum失效问题
RUN set -ex \
    && sed -i "s#/usr/bin/python#/usr/bin/python27#" /usr/bin/yum \
    && sed -i "s#/usr/bin/python#/usr/bin/python27#" /usr/libexec/urlgrabber-ext-down \
    && yum install -y deltarpm
# 基础环境配置
RUN set -ex \
    # 修改系统时区为东八区
    && rm -rf /etc/localtime \
    && ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && yum install -y vim \
    # 安装定时任务组件
    && yum -y install cronie
# pip 
COPY pip.conf  /root/.pip/pip.conf
# 安装harmonyos 相关插件
RUN pip install setuptools \
    && wget https://files.pythonhosted.org/packages/26/29/73158aec5b839234920c934ca6b81af260eaedd1f93ce341e05fa32c89b4/pycryptodome-3.9.8-cp38-cp38-manylinux1_x86_64.whl \
    && wget https://files.pythonhosted.org/packages/8a/f1/d98a89231e779b079b977590efcc31249d959c8f1d4b5858cad69695ff9c/kconfiglib-14.1.0-py2.py3-none-any.whl \
	&& wget https://files.pythonhosted.org/packages/ee/ff/48bde5c0f013094d729fe4b0316ba2a24774b3ff1c52d924a8a4cb04078a/six-1.15.0-py2.py3-none-any.whl \
	&& wget https://files.pythonhosted.org/packages/5d/8b/c6ea939f70b3356e10e771a8d5ef27fd8ecc74d9e49ec8116d94f83b9673/ecdsa-0.16.0-py2.py3-none-any.whl \
	#&& wget https://nchc.dl.sourceforge.net/project/scons/scons/3.0.5/scons-3.0.5.tar.gz \
	&& pip install pycryptodome-3.9.8-cp38-cp38-manylinux1_x86_64.whl \
	&& pip install kconfiglib-14.1.0-py2.py3-none-any.whl \
	&& pip install six-1.15.0-py2.py3-none-any.whl \
	&& pip install ecdsa-0.16.0-py2.py3-none-any.whl \
	#&& tar -zxvf scons-3.0.5.tar.gz \
	#&& cd scons-3.0.5 \
	#&& python setup.py build \
	#&& python setup.py install \
	#&& cd .. \
	&&pip install scons==4.0.1
# 支持中文
RUN yum install kde-l10n-Chinese -y
RUN localedef -c -f UTF-8 -i zh_CN zh_CN.utf8
# 更新pip版本
ENV LC_ALL zh_CN.UTF-8